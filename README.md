# SAST (Static Application Security Testing)

Read more about this feature here: https://docs.gitlab.com/ee/user/application_security/sast/

Configure SAST with CI/CD variables (https://docs.gitlab.com/ee/ci/variables/index.html).
List of available variables: https://docs.gitlab.com/ee/user/application_security/sast/index.html#available-cicd-variables

## Usage

You should add this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: gitlab.com/gitlab-components/sast@<VERSION>
```

where `<VERSION>` is the latest released tag or `main`.

If you want to leverage the `$SAST_DISABLED` variable to define or not the jobs conditionally use `include:rules`.
Otherwise all SAST jobs will always run when applicable:

```yaml
include:
  - component: gitlab.com/gitlab-components/sast@main
    rules:
      - if: $SAST_DISABLED != "true"
```

### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `test`      | The stage where you want the job to be added |
| `image_prefix` | `$CI_TEMPLATE_REGISTRY_HOST/security-products` | Define where all Docker image are pulled from |
| `image_tag` | `4` | Tag of the Docker image to use |
| `image_suffix` | `""` | Suffix added to image. If set to `-fips`, [`FIPS-enabled` images](https://docs.gitlab.com/ee/user/application_security/sast/#fips-enabled-images) are used for scan. Only used by `semgrep` analyzer |
| `excluded_analyzers` | `""` | Comma separated list of analyzers that should not run |
| `excluded_paths` | `"spec, test, tests, tmp"` | Comma separated list of paths to exclude |
| `search_max_depth` | `4` | Defines how many directory levels the search for programming languages should span |
| `run_kubesec_sast` | `"false"` | Set it to `"true"` to run `kubesec-sast` job  |
| `include_experimental` | `"false"` | Set it to `"true"` to enable [experimental analyzers](https://docs.gitlab.com/ee/user/application_security/sast/#experimental-features) |
